<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use Auth;
use App\Incidencia;

use Illuminate\Support\Facades\Storage;

class IncidenciaController extends Controller
{
    public function __construct()
    {
      //
    }

    public function createFoto($numExp)
    {
      return view('incidencia.uploadFotos', ['numExp' => $numExp]);//, 'files'=>$furl]);
    }

    public function upload($numExp, Request $request)
    {
      /*$file = $request->file('file');
      $path = $file->storeAs($numExp.'/', $file->getClientOriginalName(), 'dropbox');

      return response()->json(['success'=>$path]);*/
      $messages = [
        'required' => 'El campo :attribute es obligatorio.',
        'image' => 'Seleccione un archivo de imagen.',
        'mimes' => 'Seleccione un formato de imagen válido (png, jpg, jpeg)'
      ];
      $validator = $request->validate([
        'fotos' => 'required',
        'fotos.*' => 'required|mimes:png,jpg,jpeg|image'
      ], $messages);
      //Session::flash('warning', 'Espere mientras se cargan las fotos. Esto puede demorar un rato.');
      //if(!empty($files))
      foreach($request->file('fotos') as $file)
      {
          $path = $file->storeAs($numExp.'/', $file->getClientOriginalName(), 'dropbox');
      }
      // //return "Foto cargada en ".$path;
      Session::flash('message', 'Se cargaron las fotos con exito!');
      return view('incidencia.uploadFotos', ['numExp' => $numExp]);
    }
}
