@extends('layouts.app')
<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
@section('content')
<div class="container">
    @if (count(session('errors')) > 0)
        <div class="alert alert-danger">
          @foreach (session('errors')->all() as $error)
            {{ $error }}<br>
          @endforeach
        </div>
        @endif

        @if (session('message'))
        <div class="alert alert-success">
          {{ session('message') }}
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
          {{ session('error') }}
        </div>
        @endif
        @if (session('warning'))
        <div class="alert alert-warning">
          {{ session('warning') }}
        </div>
        @endif
          <div class="row">
              <div class="col-md-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">Cargar fotos en Dropbox</div>
                      <div class="panel-body">
                      <div class="col-md-12">
                        {!! Form::open(['action' => ['IncidenciaController@upload', 'id'=>$numExp], 'files' => true, 'enctype' => 'multipart/form-data', 'id' => 'image-upload' ]) !!}
                        <div class="form-group col-sm-12">
                          <i class="fa fa-file-image-o" aria-hidden="true"></i>
                          {!! Form::label('Fotos del incidente') !!}
                          {!! Form::file('fotos[]', ['multiple' => 'multiple']) !!}
                          <p>Para subir más de una imagen a la vez, presione control (ctrl) en el teclado y haga click en las que desea subir.</p>
                        </div>
                        <div class="form-group col-sm-12">
                          <button id="submit" class="btn btn-success"><i class="fa fa-upload"></i> Subir a Dropbox</button>
                          </div>
                        {!! Form::close() !!}
                    </div>
                    </div>
                    <div class="panel-footer">
                      <p>Este proceso puede demorar bastante, no cierre ni recergue la página.</p>
                  </div>
                  </div>
              </div>
          </div>
@endsection
